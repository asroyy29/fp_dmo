﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Spawn : NetworkBehaviour
{
    public GameObject food;
    public float speed;

    public static int totalFood = 0;

    public override void OnStartServer()
    {
        InvokeRepeating("Generate", 0, speed);
    }

    public void Generate()
    {
        int x = Random.Range(-22, 22);
        int y = Random.Range(-11, 11);

        if (totalFood < 25)
        {
            GameObject go = Instantiate(food, new Vector3(x, y, 0), Quaternion.identity);
            NetworkServer.Spawn(go);
            totalFood++;
        }
    }
}
