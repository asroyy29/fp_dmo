﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Mirror;

public class GameResult : NetworkBehaviour
{
    public static GameResult instance;
    public static int currScore;
    
    public Text scoreText;
    public Text highscoreText;

    public GameObject result;
    public GameObject playerPrefab;

    void Start()
    {
        instance = this;

        scoreText.text = currScore.ToString();

        int highscore = PlayerPrefs.GetInt("Highscore");
        if (highscore < currScore)
        {
            highscore = currScore;
            PlayerPrefs.SetInt("Highscore", highscore);
        }

        highscoreText.text = highscore.ToString();
    }

    public void ShowResult()
    {
        result.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene("Gameplay");
    }

    public void Home()
    {
        result.SetActive(false);
        SceneManager.LoadScene("Menu");
    }
}
