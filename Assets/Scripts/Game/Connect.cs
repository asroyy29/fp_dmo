﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;

public class Connect : MonoBehaviour
{
    [SerializeField] NetworkManager networkManager;

    public void StartGame()
    {
        networkManager.StartClient();
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
