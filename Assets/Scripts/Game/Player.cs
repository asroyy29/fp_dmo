﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class Player : NetworkBehaviour
{
    public float speed;
    public float increase;

    protected Joystick joystick;

    int score;
    int health;
    Rigidbody rb;

    Text scoreText;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        joystick = FindObjectOfType<Joystick>();

        score = 0;
        health = 10;
        GameResult.currScore = score;

        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        scoreText.text = "Score : " + score.ToString();
    }

    public override void OnStartLocalPlayer()
    {
        Camera.main.GetComponent<CameraFollow>().setTarget(gameObject.transform);
    }

    [Client]
    private void Update()
    {
        if (!hasAuthority) { return; }

        if (!Input.anyKey) { return; }

        if (!joystick) { return; }

        Cmd();

        if (health <= -10)
        {
            GameResult.instance.ShowResult();
            NetworkServer.Destroy(gameObject);
        }
    }

    [Command]
    void Cmd()
    {
        RpcMove();
    }

    [ClientRpc]
    void RpcMove()
    {
        Vector3 move = new Vector3(
             joystick.Horizontal * speed * Time.deltaTime,
             joystick.Vertical * speed * Time.deltaTime,
             0);
        rb.MovePosition(transform.position + move);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Food")
        {
            transform.localScale += new Vector3(increase, increase, increase);
            Destroy(other.gameObject);

            Spawn.totalFood--;
            score++;
            health++;

            scoreText.text = "Score : " + score.ToString();
        }

        if (other.gameObject.tag == "Player")
        {
            health--;
            transform.localScale += new Vector3(-increase, -increase, -increase);
        }
    }
}
